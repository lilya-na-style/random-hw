public class Main {
    public static void main(String[] args) {

        RandNumbers rand = new RandNumbers();

        rand.getRandomNum();
        rand.getTenRandNums();
        rand.getTenToZeroRandNums();
        rand.getMinusTenToTenRandNums();
        rand.getTwentyToFiftyRandNums();
        rand.getRandomRangeOfRandNums();

    }
}
